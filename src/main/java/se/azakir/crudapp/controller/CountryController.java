package se.azakir.crudapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import se.azakir.crudapp.entity.Country;
import se.azakir.crudapp.jsonresponse.restcountries.RestCountryV3;
import se.azakir.crudapp.model.CountryModelObject;
import se.azakir.crudapp.service.CountryApiService;
import se.azakir.crudapp.service.CountryHelperService;
import se.azakir.crudapp.service.CountryRestService;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/countries")
public class CountryController {

    @Autowired
    private CountryRestService countryRestService;

    @Autowired
    private CountryApiService countryApiService;

    @Autowired
    private CountryHelperService countryHelperService;

    @GetMapping("/list")
    public String listCountries(Model model) {
        List<Country> countryList = countryApiService.findAllByOrderByCountryNameAsc();
        List<CountryModelObject> countryModelObjects = countryHelperService.generateCountryModelObjects(countryList);
        model.addAttribute("countries", countryModelObjects);

        return "countries/list-countries";
    }

    @GetMapping("/addCountry")
    public String addCountry(Model model) {
        Country country = new Country();
        model.addAttribute("country", country);

        return "countries/add-country";
    }

    @GetMapping("/addCountryFromList")
    public String addCountryFromList(Model model) {
        List<RestCountryV3> restCountryV3List = countryRestService.getAllCountries();
        model.addAttribute("baseCountriesV3", restCountryV3List);

        return "countries/add-country-from-list";
    }

    @PostMapping("/saveOrUpdateCountry")
    public String saveOrUpdateCountry(@ModelAttribute("country") @Valid Country country) {
        countryApiService.save(country);

        return "redirect:/countries/list";
    }

    @PostMapping("/saveOrUpdateCountryFromList")
    public String saveOrUpdateCountryFromList(@RequestParam String nameOfCountry) {
        RestCountryV3 restCountryV3 = countryRestService.getCountry(nameOfCountry);
        Country country = new Country(restCountryV3.getCioc(), restCountryV3.getName().getCommon(), restCountryV3.getPopulation());
        countryApiService.save(country);

        return "redirect:/countries/list";
    }

    @GetMapping("/updateCountry")
    public String updateCountry(@RequestParam("countryId") int id, Model model) {
        Country country = countryApiService.findById(id).get();
        model.addAttribute("country", country);

        return "countries/add-country";
    }

    @GetMapping("/deleteCountry")
    public String deleteCountry(@RequestParam("countryId") int id) {
        countryApiService.deleteById(id);

        return "redirect:/countries/list";
    }

}
