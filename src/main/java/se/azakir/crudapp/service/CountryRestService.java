package se.azakir.crudapp.service;

import se.azakir.crudapp.jsonresponse.restcountries.RestCountryV3;

import java.util.List;

public interface CountryRestService {

    List<RestCountryV3> getAllCountries();

    RestCountryV3 getCountry(String name);

}
