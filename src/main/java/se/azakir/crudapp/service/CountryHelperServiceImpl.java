package se.azakir.crudapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.azakir.crudapp.entity.Country;
import se.azakir.crudapp.jsonresponse.restcountries.RestCountryV3;
import se.azakir.crudapp.model.CountryModelObject;

import java.util.ArrayList;
import java.util.List;

@Service
public class CountryHelperServiceImpl implements CountryHelperService {

    @Autowired
    private CountryRestService countryRestService;

    @Override
    public List<CountryModelObject> generateCountryModelObjects(List<Country> countryList) {
        List<CountryModelObject> countryModelObjects = new ArrayList<>();
        for (Country country : countryList) {
            RestCountryV3 restCountryV3 = countryRestService.getCountry(country.getCountryName());
            CountryModelObject countryModelObject = new CountryModelObject(country);
            countryModelObject.setFlagUrl(restCountryV3.getFlags().getPng());
            countryModelObjects.add(countryModelObject);
        }

        return countryModelObjects;
    }
}
