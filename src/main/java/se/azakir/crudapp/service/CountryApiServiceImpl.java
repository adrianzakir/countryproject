package se.azakir.crudapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.azakir.crudapp.dao.CountryApiDAO;
import se.azakir.crudapp.entity.Country;

import java.util.List;
import java.util.Optional;

@Service
public class CountryApiServiceImpl implements CountryApiService {

    @Autowired
    private CountryApiDAO countryApiDAO;

    @Override
    public List<Country> findAllByOrderByCountryNameAsc() {
        return countryApiDAO.findAllByOrderByCountryNameAsc();
    }

    @Override
    public void save(Country country) {
        countryApiDAO.save(country);
    }

    @Override
    public Optional<Country> findById(Integer id) {
        return countryApiDAO.findById(id);
    }

    @Override
    public void deleteById(Integer id) {
        countryApiDAO.deleteById(id);

    }

    @Override
    public void delete(Country country) {
        countryApiDAO.delete(country);
    }
}
