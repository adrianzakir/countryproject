package se.azakir.crudapp.service;

import se.azakir.crudapp.entity.Country;
import se.azakir.crudapp.model.CountryModelObject;

import java.util.List;

public interface CountryHelperService {

    List<CountryModelObject> generateCountryModelObjects(List<Country> countryList);

}
