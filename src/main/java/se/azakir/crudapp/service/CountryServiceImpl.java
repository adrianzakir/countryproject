package se.azakir.crudapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.azakir.crudapp.dao.CountryRepository;
import se.azakir.crudapp.entity.Country;

import java.util.List;
import java.util.Optional;

@Service
public class CountryServiceImpl implements CountryService {

    @Autowired
    CountryRepository countryRepository;

    @Override
    public Iterable<Country> findAll() {
        return countryRepository.findAll();
    }

    @Override
    public List<Country> findAllByOrderByCountryNameAsc() {
        return countryRepository.findAllByOrderByCountryNameAsc();
    }

    @Override
    public void save(Country country) {
        countryRepository.save(country);
    }

    @Override
    public Optional<Country> findById(Integer id) {
        return countryRepository.findById(id);
    }

    @Override
    public void deleteById(Integer id) {
        countryRepository.deleteById(id);
    }

    @Override
    public void delete(Country country) {
        countryRepository.delete(country);
    }
}
