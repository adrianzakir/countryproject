package se.azakir.crudapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.azakir.crudapp.dao.CountryRestDAO;
import se.azakir.crudapp.jsonresponse.restcountries.RestCountryV3;

import java.util.List;

@Service
public class CountryRestServiceImpl implements CountryRestService {

    @Autowired
    private CountryRestDAO countryRestDAO;

    @Override
    public List<RestCountryV3> getAllCountries() {
        return countryRestDAO.getAllCountries();
    }

    @Override
    public RestCountryV3 getCountry(String name) {
        return countryRestDAO.getCountry(name);
    }
}
