package se.azakir.crudapp.service;

import se.azakir.crudapp.entity.Person;

import java.util.Optional;


public interface PersonService {

    Iterable<Person> findAll();

    void save(Person person);

    Optional<Person> findById(Integer id);

    void deleteById(Integer id);

    void delete(Person country);

}
