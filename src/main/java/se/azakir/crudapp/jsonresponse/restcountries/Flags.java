package se.azakir.crudapp.jsonresponse.restcountries;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Flags {

    private String png;

    public String getPng() {
        return png;
    }

    public void setPng(String png) {
        this.png = png;
    }

    @Override
    public String toString() {
        return "Flags{" +
                "png='" + png + '\'' +
                '}';
    }
}
