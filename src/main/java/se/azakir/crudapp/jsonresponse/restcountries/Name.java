package se.azakir.crudapp.jsonresponse.restcountries;

public class Name {

    private String common;

    public String getCommon() {
        return common;
    }

    public void setCommon(String common) {
        this.common = common;
    }

    @Override
    public String toString() {
        return "Name{" +
                "common='" + common + '\'' +
                '}';
    }
}
