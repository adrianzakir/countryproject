package se.azakir.crudapp.jsonresponse.restcountries;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RestCountryV3 implements Comparable<RestCountryV3> {

    private Name name;
    private String cioc;
    private int population;
    private Flags flags;

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public String getCioc() {
        return cioc;
    }

    public void setCioc(String cioc) {
        this.cioc = cioc;
    }

    public Flags getFlags() {
        return flags;
    }

    public void setFlags(Flags flags) {
        this.flags = flags;
    }

    @Override
    public String toString() {
        return "RestCountryV3{" +
                "name=" + name +
                ", cioc='" + cioc + '\'' +
                ", population=" + population +
                ", flags=" + flags +
                '}';
    }

    @Override
    public int compareTo(RestCountryV3 o) {
        return this.name.getCommon().compareTo(o.getName().getCommon());
    }
}


