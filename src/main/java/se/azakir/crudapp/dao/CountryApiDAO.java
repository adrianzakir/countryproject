package se.azakir.crudapp.dao;

import se.azakir.crudapp.entity.Country;

import java.util.List;
import java.util.Optional;

public interface CountryApiDAO {

    List<Country> findAllByOrderByCountryNameAsc();

    void save(Country country);

    Optional<Country> findById(Integer id);

    void deleteById(Integer id);

    void delete(Country country);

}
