package se.azakir.crudapp.dao;

import se.azakir.crudapp.jsonresponse.restcountries.RestCountryV3;

import java.util.List;

public interface CountryRestDAO {

    List<RestCountryV3> getAllCountries();

    RestCountryV3 getCountry(String name);
}
