package se.azakir.crudapp.dao;

import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import se.azakir.crudapp.jsonresponse.restcountries.RestCountryV3;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Repository
public class CountryRestDAOImpl implements CountryRestDAO {

    private static final String ENDPOINT = "https://restcountries.com/v3.1/";

    @Override
    public List<RestCountryV3> getAllCountries() {
        RestTemplate restTemplate = new RestTemplate();
        String operation = ENDPOINT + "all?fields=name,cioc,population,flags";
        RestCountryV3[] responseCountries = restTemplate.getForObject(operation, RestCountryV3[].class);
        List<RestCountryV3> restCountryV3s = Arrays.asList(responseCountries);
        Collections.sort(restCountryV3s);

        return restCountryV3s;
    }

    @Override
    public RestCountryV3 getCountry(String name) {
        RestTemplate restTemplate = new RestTemplate();
        String operation = ENDPOINT + "name/" + name + "?fullText=true";
        RestCountryV3[] country = restTemplate.getForObject(operation, RestCountryV3[].class);

        return country[0];
    }

}
