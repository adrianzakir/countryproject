package se.azakir.crudapp.dao;

import org.springframework.data.repository.CrudRepository;
import se.azakir.crudapp.entity.Person;

public interface PersonRepository extends CrudRepository<Person, Integer> {
}
