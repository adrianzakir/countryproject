package se.azakir.crudapp.dao;

import org.springframework.data.repository.CrudRepository;
import se.azakir.crudapp.entity.Country;

import java.util.List;

public interface CountryRepository extends CrudRepository<Country, Integer> {

    List<Country> findAllByOrderByCountryNameAsc();

}
