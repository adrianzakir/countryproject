package se.azakir.crudapp.dao;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import se.azakir.crudapp.entity.Country;

import java.util.List;
import java.util.Optional;

@Repository
public class CountryApiDAOImpl implements CountryApiDAO {

    private static final String ENDPOINT = "http://localhost:8080/api/countries";

    @Override
    public List<Country> findAllByOrderByCountryNameAsc() {

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List<Country>> countryResponse =
                restTemplate.exchange(ENDPOINT,
                        HttpMethod.GET, null, new ParameterizedTypeReference<List<Country>>() {
                        });
        List<Country> countryList = countryResponse.getBody();

        return countryList;
    }

    @Override
    public void save(Country country) {
        RestTemplate restTemplate = new RestTemplate();
        if (country.getId() == 0) {
            restTemplate.postForObject(ENDPOINT, country, Country.class);
        } else if (country.getId() > 0) {
            restTemplate.put(ENDPOINT, country, Country.class);
        }
    }

    @Override
    public Optional<Country> findById(Integer id) {
        RestTemplate restTemplate = new RestTemplate();
        Optional<Country> country = Optional.ofNullable(restTemplate.getForObject(ENDPOINT + "/" + id, Country.class));

        return country;
    }

    @Override
    public void deleteById(Integer id) {
        RestTemplate template = new RestTemplate();
        template.delete(ENDPOINT + "/{id}", id);
    }

    @Override
    public void delete(Country country) {
        RestTemplate template = new RestTemplate();
        template.delete(ENDPOINT, country);
    }
}
