package se.azakir.crudapp.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import se.azakir.crudapp.entity.Person;
import se.azakir.crudapp.service.PersonService;

@RestController()
@RequestMapping("/api/people")
public class PersonRestController {

    @Autowired
    private PersonService personService;

    @GetMapping()
    public Iterable<Person> getAll(){
        return personService.findAll();
    }

    @GetMapping("/{personId}")
    public Person getPerson(@PathVariable int personId){
        return personService.findById(personId).get();
    }

    @PostMapping
    public Person addPerson(@RequestBody Person person){
        personService.save(person);

        return person;
    }

    @PutMapping
    public Person updatePerson(@RequestBody Person person){
        personService.save(person);

        return person;
    }

    @DeleteMapping("/{personId}")
    public void deletePerson(@PathVariable int personId){
        personService.deleteById(personId);
    }
}
