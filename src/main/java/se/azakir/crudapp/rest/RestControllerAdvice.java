package se.azakir.crudapp.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import se.azakir.crudapp.exception.CountryNotFoundException;
import se.azakir.crudapp.exception.ErrorMessage;

@ControllerAdvice
public class RestControllerAdvice {

    @ExceptionHandler(CountryNotFoundException.class)
    public ResponseEntity<ErrorMessage> handleCountryNotFoundException(CountryNotFoundException countryNotFoundException) {
        ErrorMessage errorMessage = new ErrorMessage(countryNotFoundException.getMessage(), HttpStatus.NOT_FOUND.value());

        return new ResponseEntity<ErrorMessage>(errorMessage, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorMessage> handleException(Exception exception) {
        ErrorMessage errorMessage = new ErrorMessage(exception.getMessage(), HttpStatus.BAD_REQUEST.value());

        return new ResponseEntity<ErrorMessage>(errorMessage, HttpStatus.BAD_REQUEST);
    }
}
