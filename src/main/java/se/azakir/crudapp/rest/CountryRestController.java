package se.azakir.crudapp.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import se.azakir.crudapp.entity.Country;
import se.azakir.crudapp.exception.CountryNotFoundException;
import se.azakir.crudapp.service.CountryService;
import se.azakir.crudapp.service.PersonService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/countries")
public class CountryRestController {

    @Autowired
    private CountryService countryRepository;
    @Autowired
    private PersonService personService;

    @GetMapping()
    public List<Country> getCountries() {
        return this.countryRepository.findAllByOrderByCountryNameAsc();
    }

    @GetMapping("/{countryId}")
    public Country getCountry(@PathVariable int countryId) {
        Optional<Country> country = this.countryRepository.findById(countryId);
        if (country.isEmpty()) {
            throw new CountryNotFoundException("Could not find country with id - " + countryId);
        }

        return country.get();
    }

    @PostMapping()
    public Country addCountry(@RequestBody @Valid Country country) {
        country.setId(0);
        countryRepository.save(country);

        return country;
    }

    @PutMapping()
    public Country updateCountry(@RequestBody Country country) {
        countryRepository.save(country);

        return country;
    }

    @DeleteMapping("/{countryId}")
    public String deleteCountry(@PathVariable int countryId) {
        countryRepository.deleteById(countryId);

        return "Country with id - " + countryId + " has been deleted";
    }

}
