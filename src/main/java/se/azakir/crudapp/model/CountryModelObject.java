package se.azakir.crudapp.model;

import se.azakir.crudapp.entity.Country;

public class CountryModelObject {

    private int id;
    private String countryName;
    private String countryCode;
    private int population;
    private String flagUrl;

    public CountryModelObject() {

    }

    public CountryModelObject(Country country) {
        this.id = country.getId();
        this.countryName = country.getCountryName();
        this.countryCode = country.getCountryCode();
        this.population = country.getPopulation();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public String getFlagUrl() {
        return flagUrl;
    }

    public void setFlagUrl(String flagUrl) {
        this.flagUrl = flagUrl;
    }
}
